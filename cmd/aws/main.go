package main

import (
	"github.com/dotronglong/goose/aws/lambda/proxy"
	"gitlab.com/era.li/go/url-api/internal/config"
)

func main() {
	proxy.Serve(config.RegisterRoutes())
}
