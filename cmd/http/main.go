package main

import (
	"fmt"
	"net/http"

	"github.com/dotronglong/goose"
	"github.com/gobeat/tools"

	"gitlab.com/era.li/go/url-api/internal/config"
)

func main() {
	goose.LoadEnv()
	tools.PanicOnError(http.ListenAndServe(fmt.Sprintf(":%d", 8080), config.RegisterRoutes()))
}
