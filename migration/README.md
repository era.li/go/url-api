## DynamoDB Migration

### Table App

```
aws dynamodb create-table \
    --table-name Era_Url \
    --attribute-definitions AttributeName=hash,AttributeType=S AttributeName=user_id,AttributeType=S \
    --key-schema AttributeName=hash,KeyType=HASH AttributeName=user_id,KeyType=RANGE \
    --global-secondary-indexes \
        IndexName=Url_GSI_01,KeySchema=[{AttributeName=id,KeyType=hash}],Projection={ProjectionType=INCLUDE,NonKeyAttributes=[href]},ProvisionedThroughput={ReadCapacityUnits=10,WriteCapacityUnits=5} \
        IndexName=Url_GSI_02,KeySchema=[{AttributeName=user_id,KeyType=hash}],Projection={ProjectionType=INCLUDE,NonKeyAttributes=[hash]},ProvisionedThroughput={ReadCapacityUnits=5,WriteCapacityUnits=5} \
    --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 \
    --endpoint-url http://127.0.0.1:28825 --region ap-southeast-1 --profile test
```

### Notes

- If we run above commands under an aws account configured (under ~/.aws),
application can only access to these tables with same credentials.
- Create a test profile `aws configure --profile test` with fake credentials
then configure app to use fake credentials