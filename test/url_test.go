package test_test

import (
	"encoding/json"
	"net/http"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/dotronglong/goose/test"
	"gitlab.com/era.li/go/url-api/internal/constant"
)

var _ = Describe("AppTest", func() {
	BeforeEach(func() {
		test.Migrate()
	})

	It("should redirect an url", func() {
		res := test.GET("/redirect/5dbeb7").Send()
		Expect(res.StatusCode).To(Equal(http.StatusMovedPermanently))
		Expect(res.Header.Get("Location")).To(Equal("https://www.era.li"))
	})

	It("should return an url only if user is owner of url", func() {
		item := make(map[string]interface{})
		res := test.GET("/urls/5dbeb7").WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("5dbeb7"))
		Expect(item["href"]).To(Equal("https://www.era.li"))
	})

	It("should return resource not found if user is anonymous", func() {
		res := test.GET("/urls/5dbeb7").Send()
		Expect(res.StatusCode).To(Equal(http.StatusNotFound))
	})

	It("should return resource not found if user is not owner of URL", func() {
		res := test.GET("/urls/5dbeb7").WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f300").Send()
		Expect(res.StatusCode).To(Equal(http.StatusNotFound))
	})

	It("should return an anonymous url", func() {
		item := make(map[string]interface{})
		res := test.GET("/urls/0c323c").SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("0c323c"))
		Expect(item["href"]).To(Equal("https://www.era.li"))
	})

	It("should create new url", func() {
		href := "https://golang.org"
		item := make(map[string]interface{})
		body, _ := json.Marshal(struct {
			Href string `json:"href"`
		}{
			Href: href,
		})
		res := test.POST("/urls").WithBody(body).SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusCreated))
		Expect(item["id"]).NotTo(BeEmpty())
		Expect(item["href"]).To(Equal(href))
	})

	It("should not create new url due to invalid href", func() {
		item := make(map[string]interface{})
		hrefs := []string{
			"http://invalid.com/perl.cgi?key= | http://web-site.com/cgi-bin/perl.cgi",
			"key1=value1&key2",
		}
		for _, href := range hrefs {
			body, _ := json.Marshal(struct {
				Href string `json:"href"`
			}{
				Href: href,
			})
			res := test.POST("/urls").WithBody(body).SendAndParse(&item)
			Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
		}
	})

	It("must prevent deleting anonymous url", func() {
		res := test.DELETE("/urls/0c323c").Send()
		Expect(res.StatusCode).To(Equal(http.StatusNotFound))
	})

	It("must prevent deleting url which not belong to current user", func() {
		res := test.DELETE("/urls/0c323c").WithHeader(constant.HeaderMockUser, "5c7d148ad137e46f1575f303").Send()
		Expect(res.StatusCode).To(Equal(http.StatusNotFound))
	})

	It("should delete an url belongs to current user", func() {
		res := test.DELETE("/urls/5dbeb7").WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").Send()
		Expect(res.StatusCode).To(Equal(http.StatusOK))
	})

	It("should allow to update an url belongs to current user", func() {
		body, _ := json.Marshal(struct {
			Title string `json:"title"`
			Id    string `json:"id"`
			Href  string `json:"href"`
		}{
			Title: "This is a title",
			Id:    "custom-id",
		})
		item := make(map[string]interface{})
		res := test.PATCH("/urls/5dbeb7").WithBody(body).
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("custom-id"))
		Expect(item["title"]).To(Equal("This is a title"))
		Expect(item["updated_at"]).NotTo(Equal(item["created_at"]))
		Expect(item["hash"]).To(Equal("f6a3d7c0d81e8d5131b65d89c024fa25"))
		Expect(item["href"]).To(Equal("https://www.era.li"))
	})

	It("should allow to update an url (href) belongs to current user", func() {
		body, _ := json.Marshal(struct {
			Title string `json:"title"`
			Id    string `json:"id"`
			Href  string `json:"href"`
		}{
			Title: "This is a title",
			Id:    "custom-id",
			Href:  "http://google.com",
		})
		item := make(map[string]interface{})
		res := test.PATCH("/urls/5dbeb7").WithBody(body).
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			WithHeader(constant.HeaderMockUserMetadata, "{\"membership\":\"premium\"}").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("custom-id"))
		Expect(item["title"]).To(Equal("This is a title"))
		Expect(item["updated_at"]).NotTo(Equal(item["created_at"]))
		Expect(item["hash"]).To(Equal("c7b920f57e553df2bb68272f61570210"))
		Expect(item["href"]).To(Equal("http://google.com"))
	})

	It("should forbid to update an url if user is not premium", func() {
		body, _ := json.Marshal(struct {
			Title string `json:"title"`
			Id    string `json:"id"`
			Href  string `json:"href"`
		}{
			Title: "This is a title",
			Id:    "custom-id",
			Href:  "http://google.com",
		})
		item := make(map[string]interface{})
		res := test.PATCH("/urls/5dbeb7").WithBody(body).
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
		Expect(item["code"]).To(Equal(constant.InvalidUserMembershipError.Code()))
	})

	It("should redirect an url without scheme to url with default scheme", func() {
		res := test.GET("/redirect/0Bm0Yy").Send()
		Expect(res.StatusCode).To(Equal(http.StatusMovedPermanently))
		Expect(res.Header.Get(constant.HeaderLocation)).To(Equal("http://www.healthysmart.com"))
	})

	It("should increase total view if get", func() {
		Skip("we switched to SQS, so this test will be skipped.")
		item := make(map[string]interface{})
		res := test.GET("/urls/5dbeb7").
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["total_view"]).To(Equal(float64(0)))

		_ = test.GET("/redirect/5dbeb7").Send()
		_ = test.GET("/redirect/5dbeb7").Send()
		res = test.GET("/urls/5dbeb7").
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["total_view"]).To(Equal(float64(2)))
	})

	It("should return urls belong to user", func() {
		data := make(map[string]interface{}, 0)
		res := test.GET("/urls").
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&data)
		Expect(res.StatusCode).To(Equal(http.StatusOK))

		pagination, ok := data["pagination"].(map[string]interface{})
		Expect(ok).To(BeTrue())
		Expect(pagination["total_items"].(float64) > 2).To(BeTrue())

		items, ok := data["items"].([]interface{})
		Expect(ok).To(BeTrue())
		for _, item := range items {
			url, ok := item.(map[string]interface{})
			Expect(ok).To(BeTrue())
			if url["id"] == "7UfHJr" {
				Expect(url["passcode"]).To(BeNil())
				Expect(url["is_locked"]).To(BeTrue())
			}
		}
	})

	It("should redirect user to passcode url for unlocking", func() {
		res := test.GET("/redirect/7UfHJr").Send()
		Expect(res.StatusCode).To(Equal(http.StatusFound))
		Expect(res.Header.Get(constant.HeaderLocation)).To(Equal("https://www.era.li/unlock/7UfHJr"))
	})

	It("should return forbidden error when accessing locked resource", func() {
		// empty or not provided passcode
		res := test.GET("/urls/7UfHJr").Send()
		Expect(res.StatusCode).To(Equal(http.StatusForbidden))

		// provide an incorrect passcode
		res = test.GET("/urls/7UfHJr?passcode=0BB").Send()
		Expect(res.StatusCode).To(Equal(http.StatusForbidden))
	})

	It("should return url data if providing correct passcode on a protected resource", func() {
		item := make(map[string]interface{})
		res := test.GET("/urls/7UfHJr?passcode=0Bm").SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("7UfHJr"))
		Expect(item["href"]).To(Equal("http://www.healthysmart.com"))
	})

	It("should return url data if current user is resource's owner", func() {
		item := make(map[string]interface{})
		res := test.GET("/urls/7UfHJr").
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("7UfHJr"))
		Expect(item["href"]).To(Equal("http://www.healthysmart.com"))
		Expect(item["passcode"]).To(BeNil())
		Expect(item["is_locked"]).To(BeTrue())
	})

	It("should not allow to create url with passcode", func() {
		// passcode is only able to created after url created
		href := "https://golang.org"
		passcode := "123456"
		item := make(map[string]interface{})
		body, _ := json.Marshal(struct {
			Href     string `json:"href"`
			Passcode string `json:"passcode"`
		}{
			Href:     href,
			Passcode: passcode,
		})
		res := test.POST("/urls").
			WithBody(body).
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusCreated))
		Expect(item["id"]).NotTo(BeEmpty())
		Expect(item["href"]).To(Equal(href))
		Expect(item["passcode"]).To(BeNil())
		Expect(item["is_locked"]).To(BeFalse())
	})

	It("should allow to remove passcode from url for premium members", func() {
		item := make(map[string]interface{})
		res := test.DELETE("/urls/7UfHJr/passcode").
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			WithHeader(constant.HeaderMockUserMetadata, "{\"membership\":\"premium\"}").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("7UfHJr"))
		Expect(item["href"]).To(Equal("http://www.healthysmart.com"))
		Expect(item["passcode"]).To(BeNil())
		Expect(item["is_locked"]).To(BeFalse())
	})

	It("should forbid to remove passcode from url for non-premium members", func() {
		item := make(map[string]interface{})
		res := test.DELETE("/urls/7UfHJr/passcode").
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
		Expect(item["code"]).To(Equal(constant.InvalidUserMembershipError.Code()))
	})

	It("should allow to create passcode to url for premium members", func() {
		item := make(map[string]interface{})
		body, _ := json.Marshal(struct {
			ID       string `json:"id"`
			Passcode string `json:"passcode"`
		}{
			ID:       "BPAGsZ",
			Passcode: "024680",
		})
		res := test.POST("/urls/BPAGsZ/passcode").
			WithBody(body).
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			WithHeader(constant.HeaderMockUserMetadata, "{\"membership\":\"premium\"}").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusOK))
		Expect(item["id"]).To(Equal("BPAGsZ"))
		Expect(item["href"]).To(Equal("https://www.youtube.com/watch?v=ZJ70YS9Z340"))
		Expect(item["passcode"]).To(BeNil())
		Expect(item["is_locked"]).To(BeTrue())
	})

	It("should forbid to create passcode to url for non-premium members", func() {
		item := make(map[string]interface{})
		body, _ := json.Marshal(struct {
			ID       string `json:"id"`
			Passcode string `json:"passcode"`
		}{
			ID:       "BPAGsZ",
			Passcode: "024680",
		})
		res := test.POST("/urls/BPAGsZ/passcode").
			WithBody(body).
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
		Expect(item["code"]).To(Equal(constant.InvalidUserMembershipError.Code()))
	})

	It("should reject passcode which is not satisfied [0-9]{6}", func() {
		item := make(map[string]interface{})
		body, _ := json.Marshal(struct {
			ID       string `json:"id"`
			Passcode string `json:"passcode"`
		}{
			ID:       "BPAGsZ",
			Passcode: "0Bm",
		})
		res := test.POST("/urls/BPAGsZ/passcode").
			WithBody(body).
			WithHeader(constant.HeaderMockUser, "5c7d149ed137e46f1575f305").
			SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(http.StatusBadRequest))
	})

	It("should reject unsafe url when creating", func() {
		Skip("this domain is green at this moment")
		item := make(map[string]interface{})
		body, _ := json.Marshal(struct {
			Href string `json:"href"`
		}{
			Href: "https://inviteationtalking.tk/Chase",
		})
		res := test.POST("/urls").WithBody(body).SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(constant.UnsafeUrlError.Status()))
		Expect(item["code"]).To(Equal(constant.UnsafeUrlError.Code()))
	})

	It("should reject unsafe url when updating", func() {
		Skip("this domain is green at this moment")
		item := make(map[string]interface{})
		header := http.Header{}
		header.Set(constant.HeaderMockUser, "5c7d149ed137e46f1575f305")
		header.Set(constant.HeaderMockUserMetadata, "{\"membership\":\"premium\"}")
		body, _ := json.Marshal(struct {
			Id   string `json:"id"`
			Href string `json:"href"`
		}{
			Id:   "BPAGsZ",
			Href: "https://inviteationtalking.tk/Chase",
		})
		res := test.PATCH("/urls/BPAGsZ").WithBody(body).SendAndParse(&item)
		Expect(res.StatusCode).To(Equal(constant.UnsafeUrlError.Status()))
		Expect(item["code"]).To(Equal(constant.UnsafeUrlError.Code()))
	})

	It("should redirect to webapp's url if resource could not be found", func() {
		res := test.GET("/redirect/some-magic-links").Send()
		Expect(res.StatusCode).To(Equal(http.StatusFound))
		Expect(res.Header.Get(constant.HeaderLocation)).To(Equal("https://www.era.li"))
	})
})
