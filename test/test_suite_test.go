package test_test

import (
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"github.com/dotronglong/goose"
	"github.com/dotronglong/goose/iam"
	"github.com/dotronglong/goose/test"
	"github.com/gobeat/tools"
	"github.com/joho/godotenv"
	"gitlab.com/era.li/go/url-api/internal/config"
	"gitlab.com/era.li/go/url-api/internal/constant"
	"gitlab.com/era.li/go/url-api/internal/service"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestTest(t *testing.T) {
	tools.PanicOnError(godotenv.Load("../.env.test"))
	handler := config.RegisterRoutesWithRouter(goose.RouterWithCors(nil).BeforeDispatch(func(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
		if r.Method == http.MethodOptions {
			return nil, nil
		}
		if goose.Env().GetString(constant.KeyAppEnv) == constant.ValueAppTest {
			metadata := tools.NewBag()
			if mu := r.Header.Get(constant.HeaderMockUserMetadata); mu != "" {
				m := make(map[string]interface{})
				err := json.Unmarshal([]byte(mu), &m)
				if err == nil {
					for k, v := range m {
						metadata.Set(k, v)
					}
				}
			}
			if mu := r.Header.Get(constant.HeaderMockUser); mu != "" {
				// in test env, accepts header mock-user
				return iam.Identity().WithUser(r, iam.NewUser(mu, "", true, true, metadata)).Context(), nil
			}
		}

		return iam.Identity().WithUser(r, iam.NewUser(constant.ValueUserAnonymous, "", true, true, nil)).Context(), nil
	}))
	test.Init().WithHandler(handler).WithMigrator(service.NewMigrator(".."))
	RegisterFailHandler(Fail)
	RunSpecs(t, "Test Suite")
}
