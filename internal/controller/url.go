package controller

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/dotronglong/goose"
	"github.com/dotronglong/goose/iam"
	"github.com/gobeat/router"

	"gitlab.com/era.li/go/url-api/internal/constant"
	"gitlab.com/era.li/go/url-api/internal/entity"
	"gitlab.com/era.li/go/url-api/internal/repository"
	"gitlab.com/era.li/go/url-api/internal/service"
)

type UrlController struct{}

func (c *UrlController) All(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	q := r.URL.Query().Get(constant.KeyQ)
	page, err := strconv.Atoi(r.URL.Query().Get(constant.KeyPage))
	if err != nil || page < 1 {
		page = 1
	}
	size, err := strconv.Atoi(r.URL.Query().Get(constant.KeySize))
	if err != nil || size < 1 {
		size = 10
	}
	urls, pagination, err := repository.UrlRepositoryInstance().All(iam.Identity().User(r).ID(), q, page, size)
	if err != nil {
		return nil, err
	}
	for _, url := range urls {
		url.IsLocked = url.HasPasscode()
	}
	payload := make(map[string]interface{})
	payload[constant.KeyPagination] = pagination
	payload[constant.KeyItems] = urls
	return goose.Reply(r).WithBody(payload).Context(), nil
}

func (c *UrlController) Redirect(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	url, err := c.getUrlForRedirect(r)
	if err != nil {
		if err == constant.ResourceNotFoundError {
			webappRedirectUrl := goose.Env().GetString(constant.KeyWebappRedirectUrl)
			return goose.Reply(r).WithStatus(http.StatusFound).WithHeader(constant.HeaderLocation, webappRedirectUrl).Context(), nil
		}
		return nil, err
	}
	href := url.Href
	var redirectURL string
	willCount := true
	response := goose.Reply(r)
	response.WithStatus(http.StatusMovedPermanently)
	if url.HasPasscode() {
		redirectURL = goose.Env().GetString(constant.KeyPasscodeRedirectUrl)
		if redirectURL != "" {
			href = fmt.Sprintf("%s%s", redirectURL, url.Id)
			willCount = false
			response.WithStatus(http.StatusFound)
		}
	}
	if !strings.HasPrefix(href, "http") {
		href = fmt.Sprintf("%s%s", "http://", href)
	}
	response.WithHeader(constant.HeaderLocation, href)
	response.WithHeader(constant.HeaderContentType, "text/html; charset=utf-8")
	if r.Method == http.MethodGet {
		response.WithBody(fmt.Sprintf("<html><head><title>era.li</title></head><body><a href=\"%s\">moved here</a></body></html>", href))
		if willCount && !url.IsAnonymous() {
			defer c.increaseUrlView(url)
		}
	}

	return response.Context(), err
}

func (c *UrlController) Get(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	url, err := c.getUrl(r)
	if err != nil {
		return nil, err
	}

	user := iam.Identity().User(r)
	if user != nil && url.BelongsTo(user.ID()) {
		// fine, ignore this case
		return goose.Reply(r).WithBody(url).Context(), nil
	}

	passcode := r.URL.Query().Get(constant.KeyRequestPasscode)
	if url.HasPasscode() {
		if !url.Unlock(passcode) {
			// resource is locked and accessing by an empty passcode,
			// or passcode is incorrect, returns forbidden error
			return nil, constant.ResourceForbiddenError
		}

		// since this is an unlocking passcode, increase total view
		defer c.increaseUrlView(url)
	} else if !url.HasPasscode() && !url.IsAnonymous() && (user == nil || !url.BelongsTo(user.ID())) {
		// if url is not locked, and this url is not an anonymous url,
		// we will check whether or not current user is owner of resource,
		// returns resource not found when current user is not owner.
		return nil, constant.ResourceNotFoundError
	}

	return goose.Reply(r).WithBody(url).Context(), nil
}

func (c *UrlController) Create(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	p := new(urlCreatePayload)
	err = goose.Validate(r, p)
	if err != nil {
		return nil, err
	}
	err = c.validateHref(p.Href)
	if err != nil {
		return nil, err
	}
	err = service.ValidateSafeBrowsingUrl(p.Href)
	if err != nil {
		return nil, err
	}
	hash := goose.Md5(p.Href)
	var userID string
	user := iam.Identity().User(r)
	if user != nil {
		userID = user.ID()
	} else {
		userID = constant.ValueUserAnonymous
	}
	uu, err := repository.UrlRepositoryInstance().FindByHashAndUserId(hash, userID)
	if err != nil {
		return nil, err
	}
	if uu != nil {
		return goose.Reply(r).WithStatus(http.StatusCreated).WithBody(uu).Context(), nil
	}
	url := entity.NewUrl()
	url.Href = p.Href
	url.Hash = goose.Md5(p.Href)
	url.UserId = userID
	url.Id, err = repository.UrlRepositoryInstance().GenerateUrlId(int(goose.Env().GetInt(constant.KeyUrlIdLength)))
	if err != nil {
		return nil, err
	}
	now := time.Now().Unix()
	url.CreatedAt = now
	url.UpdatedAt = now
	err = repository.UrlRepositoryInstance().Create(url)
	if err != nil {
		return nil, err
	}
	return goose.Reply(r).WithStatus(http.StatusCreated).WithBody(url).Context(), nil
}

func (c *UrlController) Update(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	p := new(urlUpdatePayload)
	err = goose.Validate(r, p)
	if err != nil {
		return nil, err
	}
	url, err := c.getUrlForUser(r)
	if err != nil {
		return nil, err
	}
	if p.Id != "" && p.Id != url.Id {
		// TODO: verify permissions
		// verify id's existence
		if uu, err := repository.UrlRepositoryInstance().Get(p.Id); err != nil || uu != nil {
			return nil, constant.DuplicateUrlIdError
		}
		repository.UrlRepositoryInstance().Delete(url.Id)
		url.Id = p.Id
	}
	url.Title = p.Title
	if p.Href != "" && p.Href != url.Href {
		// If user wants to update new Href
		// then check if href already exists in user's account
		// throw an error if exists
		err = c.validateHref(p.Href)
		if err != nil {
			return nil, err
		}
		user := iam.Identity().User(r)
		if user != nil && user.Metadata().GetString(constant.KeyUserMembership) != constant.ValueMembershipPremium {
			return nil, constant.InvalidUserMembershipError
		}
		err := service.ValidateSafeBrowsingUrl(p.Href)
		if err != nil {
			return nil, err
		}
		url.Href = p.Href
		url.Hash = goose.Md5(p.Href)
	}
	url.UpdatedAt = time.Now().Unix()
	err = repository.UrlRepositoryInstance().Update(url.Id, url)
	if err != nil {
		return nil, err
	}

	return goose.Reply(r).WithBody(url).Context(), nil
}

func (c *UrlController) Delete(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	url, err := c.getUrlForUser(r)
	if err != nil {
		return nil, err
	}
	repository.UrlRepositoryInstance().Delete(url.Id)

	return goose.Reply(r).WithBody(url).Context(), nil
}

func (c *UrlController) CreatePasscode(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	p := new(urlCreatePasscodePayload)
	err = goose.Validate(r, p)
	if err != nil {
		return nil, err
	}
	user := iam.Identity().User(r)
	if user != nil && user.Metadata().GetString(constant.KeyUserMembership) != constant.ValueMembershipPremium {
		return nil, constant.InvalidUserMembershipError
	}
	url, err := c.getUrlForUser(r)
	if err != nil {
		return nil, err
	}
	url.Passcode = p.Passcode
	url.UpdatedAt = time.Now().Unix()
	err = repository.UrlRepositoryInstance().Update(url.Id, url)
	if err != nil {
		return nil, err
	}
	url.IsLocked = url.HasPasscode()

	return goose.Reply(r).WithBody(url).Context(), nil
}

func (c *UrlController) DeletePasscode(w http.ResponseWriter, r *http.Request) (ctx context.Context, err error) {
	user := iam.Identity().User(r)
	if user != nil && user.Metadata().GetString(constant.KeyUserMembership) != constant.ValueMembershipPremium {
		return nil, constant.InvalidUserMembershipError
	}
	url, err := c.getUrlForUser(r)
	if err != nil {
		return nil, err
	}
	url.Passcode = ""
	url.UpdatedAt = time.Now().Unix()
	err = repository.UrlRepositoryInstance().Update(url.Id, url)
	if err != nil {
		return nil, err
	}
	url.IsLocked = url.HasPasscode()

	return goose.Reply(r).WithBody(url).Context(), nil
}

func (c *UrlController) getUrl(r *http.Request) (url *entity.Url, err error) {
	url, err = repository.UrlRepositoryInstance().Get(router.RequestBag(r).GetString(constant.KeyId))
	if err != nil {
		return nil, err
	} else if url == nil {
		return nil, constant.ResourceNotFoundError
	}
	url.IsLocked = url.HasPasscode()

	return url, nil
}

func (c *UrlController) getUrlForUser(r *http.Request) (url *entity.Url, err error) {
	url, err = c.getUrl(r)
	if err != nil {
		return nil, err
	}
	if user := iam.Identity().User(r); user == nil || !url.BelongsTo(user.ID()) {
		return nil, constant.ResourceNotFoundError
	}
	return url, nil
}

func (c *UrlController) getUrlForRedirect(r *http.Request) (url *entity.Url, err error) {
	url, err = repository.UrlRepositoryInstance().GetRedirect(router.RequestBag(r).GetString(constant.KeyId))
	if err != nil {
		return nil, err
	} else if url == nil {
		return nil, constant.ResourceNotFoundError
	}
	return url, nil
}

func (c *UrlController) validateHref(href string) error {
	validator := regexp.MustCompile(`^(https?|ftp)://(-\.)?([^\s/?\.#-]+\.?)+(/[^\s]*)?$`)
	if validator.MatchString(href) {
		return nil
	}
	return constant.InvalidHrefError
}

func (c *UrlController) increaseUrlView(url *entity.Url) {
	err := repository.UrlRepositoryInstance().IncreaseTotalView(url.Id, url)
	if err != nil {
		fmt.Println(err)
	}
}
