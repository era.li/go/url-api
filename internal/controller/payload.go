package controller

type urlCreatePayload struct {
	Href string `json:"href"`
}

type urlUpdatePayload struct {
	Id    string `json:"id,omitempty" validate:"minLength=2;maxLength=20"`
	Title string `json:"title" validate:"minLength=0;maxLength=50"`
	Href  string `json:"href"`
}

type urlCreatePasscodePayload struct {
	Passcode string `json:"passcode" validate:"regexp=[0-9]{6}"`
}
