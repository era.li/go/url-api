package firestore

import (
	"context"

	"cloud.google.com/go/firestore"
	"github.com/dotronglong/goose"
	"github.com/gobeat/tools"
	"google.golang.org/api/option"

	"gitlab.com/era.li/go/url-api/internal/constant"
)

var (
	d *UrlRepositoryFirestore
)

// DBInstance returns an instance of repository.UrlRepository
func DBInstance() *UrlRepositoryFirestore {
	if d == nil {
		ctx := context.Background()
		projectID := goose.Env().GetString(constant.KeyGcpProject)
		credFile := goose.Env().GetString("GOOGLE_APPLICATION_CREDENTIALS")
		var client *firestore.Client
		var err error
		if credFile != "" {
			client, err = firestore.NewClient(ctx, projectID, option.WithCredentialsFile(credFile))
		} else {
			client, err = firestore.NewClient(ctx, projectID)
		}

		tools.PanicOnError(err)
		uc := client.Collection("urls")
		d = &UrlRepositoryFirestore{ctx, uc}
	}

	return d
}
