package firestore

import (
	"context"
	"strings"

	"cloud.google.com/go/firestore"
	"github.com/dotronglong/goose"
	"gitlab.com/era.li/go/url-api/internal/constant"
	"gitlab.com/era.li/go/url-api/internal/entity"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UrlRepositoryFirestore struct {
	ctx context.Context
	uc  *firestore.CollectionRef
}

func (r UrlRepositoryFirestore) GenerateUrlId(len int) (id string, err error) {
	for {
		id = goose.RandomString(len, true, true, true, false)
		url, err := r.GetRedirect(id)
		if err != nil {
			return "", constant.UnableToGenerateIdError
		} else if url == nil {
			return id, nil
		}
	}
}

func (r UrlRepositoryFirestore) GetRedirect(id string) (*entity.Url, error) {
	return r.Get(id)
}

func (r UrlRepositoryFirestore) FindByHashAndUserId(hash string, userId string) (*entity.Url, error) {
	iter := r.uc.Where("hash", "==", hash).Where("user_id", "==", userId).Limit(1).Documents(r.ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}

		return entity.NewUrlWithValues(doc.Data()), nil
	}

	return nil, nil
}

func (r UrlRepositoryFirestore) Get(id string) (*entity.Url, error) {
	doc, err := r.uc.Doc(id).Get(r.ctx)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return nil, nil
		}

		return nil, err
	}

	return entity.NewUrlWithValues(doc.Data()), nil
}

func (r UrlRepositoryFirestore) Create(url *entity.Url) error {
	_, err := r.uc.Doc(url.Id).Set(r.ctx, url.ToMap())
	if err != nil {
		return err
	}

	return nil
}

func (r UrlRepositoryFirestore) Update(id string, url *entity.Url) error {
	return r.Create(url)
}

func (r UrlRepositoryFirestore) Delete(id string) error {
	_, err := r.uc.Doc(id).Delete(r.ctx)
	if err != nil {
		return err
	}

	return nil
}

func (r UrlRepositoryFirestore) All(userId string, q string, page int, size int) ([]*entity.Url, *entity.Pagination, error) {
	// TODO: improve using StartAt & EndAt
	// fetch all user's urls (only applicable since the data is still small)
	urls := make([]*entity.Url, 0)
	iter := r.uc.Where("user_id", "==", userId).Documents(r.ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, nil, err
		}

		url := entity.NewUrlWithValues(doc.Data())
		if q != "" && !strings.Contains(url.Title, q) {
			continue
		}

		urls = append(urls, url)
	}

	pagination := &entity.Pagination{
		Page: page,
		Size: size,
	}
	pagination.SetTotalItems(len(urls))

	start, end := r.paginate(pagination.TotalItems, page, size)
	if start < 0 {
		return make([]*entity.Url, 0), pagination, nil
	}
	urls = urls[start:end]

	return urls, pagination, nil
}

func (r UrlRepositoryFirestore) IncreaseTotalView(id string, url *entity.Url) error {
	_, err := r.uc.Doc(id).Update(r.ctx, []firestore.Update{
		{Path: "total_view", Value: firestore.Increment(1)},
	})

	return err
}

func (r UrlRepositoryFirestore) paginate(count int, page int, size int) (start int, end int) {
	start = (page - 1) * size
	if start > count-1 {
		return -1, 0
	}
	end = start + size
	if end > count {
		end = count
	}
	return start, end
}
