package dynamodb

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gobeat/tools"
	"github.com/dotronglong/goose"
	"gitlab.com/era.li/go/url-api/internal/constant"
)

var (
	d *dynamodb.DynamoDB
)

// DBInstance returns an instance of dynamodb.DynamoDB
func DBInstance() *dynamodb.DynamoDB {
	if d == nil {
		accessID := goose.Env().GetString(constant.KeyAwsDynamoAccessId)
		accessSecret := goose.Env().GetString(constant.KeyAwsDynamoAccessSecret)
		ss, err := session.NewSession(&aws.Config{
			Region:                        aws.String(goose.Env().GetString(constant.KeyAwsRegion)),
			Endpoint:                      aws.String(goose.Env().GetString(constant.KeyAwsDynamoEndpoint)),
			CredentialsChainVerboseErrors: aws.Bool(true),
			MaxRetries:                    aws.Int(1),
		})
		if accessID != "" && accessSecret != "" {
			ss.Config.WithCredentials(credentials.NewStaticCredentials(accessID, accessSecret, ""))
		}
		tools.PanicOnError(err)
		d = dynamodb.New(ss)
	}

	return d
}
