package dynamodb

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/dotronglong/goose"
	"gitlab.com/era.li/go/url-api/internal/constant"
	"gitlab.com/era.li/go/url-api/internal/entity"
)

type UrlRepositoryDynamoDB struct{}

func (r UrlRepositoryDynamoDB) GenerateUrlId(len int) (id string, err error) {
	for {
		id = goose.RandomString(len, true, true, true, false)
		url, err := r.GetRedirect(id)
		if err != nil {
			return "", constant.UnableToGenerateIdError
		} else if url == nil {
			return id, nil
		}
	}
}

func (r UrlRepositoryDynamoDB) GetRedirect(id string) (*entity.Url, error) {
	in := &dynamodb.QueryInput{
		TableName:              aws.String(GetTableUrl()),
		IndexName:              aws.String(UrlGsi06),
		KeyConditionExpression: aws.String("id = :id"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":id": {
				S: aws.String(id),
			},
		},
		Limit: aws.Int64(1),
	}
	out, err := DBInstance().Query(in)
	if err != nil {
		return nil, err
	} else if *out.Count < 1 {
		return nil, nil
	}
	url := entity.NewUrl()
	err = dynamodbattribute.UnmarshalMap(out.Items[0], url)
	return url, err
}

func (r UrlRepositoryDynamoDB) FindByHashAndUserId(hash string, userId string) (*entity.Url, error) {
	in := &dynamodb.GetItemInput{
		TableName: aws.String(GetTableUrl()),
		Key: map[string]*dynamodb.AttributeValue{
			"hash": {
				S: aws.String(hash),
			},
			"user_id": {
				S: aws.String(userId),
			},
		},
	}
	out, err := DBInstance().GetItem(in)
	if err != nil {
		return nil, err
	}
	if out.Item == nil {
		return nil, nil
	}
	url := entity.NewUrl()
	err = dynamodbattribute.UnmarshalMap(out.Item, url)
	return url, err
}

func (r UrlRepositoryDynamoDB) Get(id string) (*entity.Url, error) {
	url, err := r.GetRedirect(id)
	if err != nil {
		return nil, err
	}
	if url == nil {
		return nil, nil
	}
	return r.FindByHashAndUserId(url.Hash, url.UserId)
}

func (r UrlRepositoryDynamoDB) Create(url *entity.Url) error {
	item, err := dynamodbattribute.MarshalMap(url)
	if err != nil {
		return err
	}
	in := &dynamodb.PutItemInput{
		TableName: aws.String(GetTableUrl()),
		Item:      item,
	}
	_, err = DBInstance().PutItem(in)
	return err
}

func (r UrlRepositoryDynamoDB) Update(id string, url *entity.Url) error {
	return r.Create(url)
}

func (r UrlRepositoryDynamoDB) Delete(hash string, userId string) error {
	in := &dynamodb.DeleteItemInput{
		TableName: aws.String(GetTableUrl()),
		Key: map[string]*dynamodb.AttributeValue{
			"hash":    {S: aws.String(hash)},
			"user_id": {S: aws.String(userId)},
		},
	}
	_, err := DBInstance().DeleteItem(in)
	return err
}

func (r UrlRepositoryDynamoDB) All(userId string, q string, page int, size int) ([]*entity.Url, *entity.Pagination, error) {
	pagination := &entity.Pagination{
		Page: page,
		Size: size,
	}
	urls, err := r.getAllUserUrlsFilteringByTitle(userId, q)
	if err != nil {
		return nil, pagination, err
	}
	pagination.SetTotalItems(len(urls))
	start, end := r.paginate(pagination.TotalItems, page, size)
	if start < 0 {
		return make([]*entity.Url, 0), pagination, nil
	}
	urls = urls[start:end]
	if len(urls) > 0 {
		requestKeysAndAttributes := new(dynamodb.KeysAndAttributes)
		for _, url := range urls {
			requestKeysAndAttributes.Keys = append(requestKeysAndAttributes.Keys, map[string]*dynamodb.AttributeValue{
				"hash":    {S: aws.String(url.Hash)},
				"user_id": {S: aws.String(url.UserId)},
			})
		}
		requestItems := make(map[string]*dynamodb.KeysAndAttributes)
		requestItems[GetTableUrl()] = requestKeysAndAttributes
		in := &dynamodb.BatchGetItemInput{
			RequestItems: requestItems,
		}
		out, err := DBInstance().BatchGetItem(in)
		if err != nil {
			return nil, pagination, err
		}
		err = dynamodbattribute.UnmarshalListOfMaps(out.Responses[GetTableUrl()], &urls)
	}
	return urls, pagination, err
}

func (r UrlRepositoryDynamoDB) paginate(count int, page int, size int) (start int, end int) {
	start = (page - 1) * size
	if start > count-1 {
		return -1, 0
	}
	end = start + size
	if end > count {
		end = count
	}
	return start, end
}

func (r UrlRepositoryDynamoDB) getAllUserUrlsFilteringByTitle(userId string, title string) ([]*entity.Url, error) {
	// loop to query a number {size} of items based on userId and Title
	// loop until no more results to be fetched
	// any function which uses result of this function should perform pagination,
	// since the returning data might be large
	size := 100
	urls := make([]*entity.Url, 0)
	var lastEvaluatedKey map[string]*dynamodb.AttributeValue
	for {
		in := &dynamodb.QueryInput{
			TableName:              aws.String(GetTableUrl()),
			IndexName:              aws.String(UrlGsi05),
			KeyConditionExpression: aws.String("user_id = :user_id"),
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":user_id": {S: aws.String(userId)},
			},
			Limit:            aws.Int64(int64(size)),
			ScanIndexForward: aws.Bool(false),
		}
		if title != "" {
			in.SetFilterExpression("contains(title, :title)")
			in.ExpressionAttributeValues[":title"] = &dynamodb.AttributeValue{S: aws.String(title)}
		}
		if lastEvaluatedKey != nil {
			in.SetExclusiveStartKey(lastEvaluatedKey)
		}
		out, err := DBInstance().Query(in)
		if err != nil {
			return nil, err
		}
		items := make([]*entity.Url, 0)
		err = dynamodbattribute.UnmarshalListOfMaps(out.Items, &items)
		if err != nil {
			return nil, err
		}
		urls = append(urls, items...)
		if out.LastEvaluatedKey == nil {
			break
		}
		lastEvaluatedKey = out.LastEvaluatedKey
		// TODO: define a better mechanism to prevent this loop possibly turns into infinite loop
	}
	return urls, nil
}
