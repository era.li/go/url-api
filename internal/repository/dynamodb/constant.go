package dynamodb

import (
	"fmt"

	"github.com/dotronglong/goose"
)

const (
	TableUrl = "Url"
	UrlGsi05 = "Url_GSI_05"
	UrlGsi06 = "Url_GSI_06"
)

var tblPrefix string

func GetTable(name string) string {
	if tblPrefix == "" {
		tblPrefix = goose.Env().GetString("TABLE_PREFIX")
		if tblPrefix == "" {
			tblPrefix = "Era_"
		}
	}
	return fmt.Sprintf("%s%s", tblPrefix, name)
}

func GetTableUrl() string {
	return GetTable(TableUrl)
}
