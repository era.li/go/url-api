package repository

import (
	. "gitlab.com/era.li/go/url-api/internal/entity"
	. "gitlab.com/era.li/go/url-api/internal/repository/firestore"
)

var (
	ur UrlRepository
)

func UrlRepositoryInstance() UrlRepository {
	if ur == nil {
		ur = DBInstance()
	}

	return ur
}

type UrlRepository interface {
	GenerateUrlId(len int) (id string, err error)
	Get(id string) (*Url, error)
	GetRedirect(id string) (*Url, error)
	Create(url *Url) error
	Update(id string, url *Url) error
	Delete(id string) error
	FindByHashAndUserId(hash string, userId string) (*Url, error)
	All(userId string, q string, page int, size int) ([]*Url, *Pagination, error)
	IncreaseTotalView(id string, url *Url) error
}
