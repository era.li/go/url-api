package entity

import (
	"github.com/gobeat/tools"

	"gitlab.com/era.li/go/url-api/internal/constant"
)

func NewUrl() *Url {
	return &Url{
		TotalView: 0,
	}
}

func NewUrlWithValues(data map[string]interface{}) *Url {
	url := NewUrl()
	bag := tools.NewBagWithValues(data)
	url.Id = bag.GetString("id")
	url.Hash = bag.GetString("hash")
	url.Href = bag.GetString("href")
	url.UserId = bag.GetString("user_id")
	url.Title = bag.GetString("title")
	url.Passcode = bag.GetString("passcode")
	url.IsLocked = url.HasPasscode()
	url.CreatedAt = bag.GetInt("created_at")
	url.UpdatedAt = bag.GetInt("updated_at")
	url.TotalView = bag.GetInt("total_view")

	return url
}

type Url struct {
	Id        string `json:"id"`
	Hash      string `json:"hash"`
	Href      string `json:"href"`
	UserId    string `json:"-" dynamodbav:"user_id"`
	Title     string `json:"title"`
	TotalView int64  `json:"total_view"`
	Passcode  string `json:"-" dynamodbav:"passcode"`
	IsLocked  bool   `json:"is_locked" dynamodbav:"-" firestore:"-"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
}

// toMap converts url to map
func (u *Url) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"id":         u.Id,
		"hash":       u.Hash,
		"href":       u.Href,
		"user_id":    u.UserId,
		"title":      u.Title,
		"total_view": u.TotalView,
		"passcode":   u.Passcode,
		"created_at": u.CreatedAt,
		"updated_at": u.UpdatedAt,
	}
}

// BelongsTo returns true if provided user is url's owner
func (u *Url) BelongsTo(userId string) bool {
	return u.UserId != constant.ValueUserAnonymous && u.UserId == userId
}

// IsAnonymous returns true if this url is an anonymous
func (u *Url) IsAnonymous() bool {
	return u.UserId == constant.ValueUserAnonymous
}

// HasPasscode returns true if url contains a passcode
func (u *Url) HasPasscode() bool {
	return u.Passcode != ""
}

// Unlock returns true if passcode is correct
func (u *Url) Unlock(passcode string) bool {
	return passcode != "" && u.Passcode == passcode
}
