package entity

type Pagination struct {
	TotalPages int `json:"total_pages"`
	TotalItems int `json:"total_items"`
	Page       int `json:"page"`
	Size       int `json:"size"`
}

func (p *Pagination) SetTotalItems(totalItems int) {
	p.TotalItems = totalItems
	p.TotalPages = p.TotalItems / p.Size
	if p.TotalPages*p.Size < p.TotalItems {
		p.TotalPages++
	}
}
