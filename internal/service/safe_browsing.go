package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/dotronglong/goose"
	"gitlab.com/era.li/go/url-api/internal/constant"
)

var client *http.Client

func init() {
	client = &http.Client{}
}

func ValidateSafeBrowsingUrl(href string) error {
	apiKey := goose.Env().GetOrDefault(constant.KeySafeBrowsingApiKey, "")
	if apiKey == "" {
		return nil
	}
	url := fmt.Sprintf("https://safebrowsing.googleapis.com/v4/threatMatches:find?key=%s", apiKey)
	body, err := json.Marshal(safeBrowsingRequestBody{
		Client: safeBrowsingRequestBodyClient{
			ClientId:      "era.li",
			ClientVersion: "1.5.2",
		},
		ThreatInfo: safeBrowsingRequestBodyThreatInfo{
			ThreatTypes:      []string{"MALWARE", "SOCIAL_ENGINEERING"},
			PlatformTypes:    []string{"WINDOWS"},
			ThreatEntryTypes: []string{"URL"},
			ThreatEntries: []safeBrowsingRequestBodyThreatEntry{
				{Url: href},
			},
		},
	})
	if err != nil {
		fmt.Println(err)
		return constant.InvalidSafeUrlError
	}
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		fmt.Println(err)
		return constant.InvalidSafeUrlError
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	defer response.Body.Close()
	if err != nil {
		fmt.Println(err)
		return constant.InvalidSafeUrlError
	}
	if response.StatusCode != http.StatusOK {
		fmt.Println("Invalid response status", response.StatusCode)
		return constant.InvalidSafeUrlError
	}
	body, err = ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		return constant.InvalidSafeUrlError
	}
	if string(body) != "{}\n" {
		return constant.UnsafeUrlError
	}

	return nil
}

type safeBrowsingRequestBody struct {
	Client     safeBrowsingRequestBodyClient     `json:"client"`
	ThreatInfo safeBrowsingRequestBodyThreatInfo `json:"threatInfo"`
}

type safeBrowsingRequestBodyClient struct {
	ClientId      string `json:"clientId"`
	ClientVersion string `json:"clientVersion"`
}

type safeBrowsingRequestBodyThreatInfo struct {
	ThreatTypes      []string                             `json:"threatTypes"`
	PlatformTypes    []string                             `json:"platformTypes"`
	ThreatEntryTypes []string                             `json:"threatEntryTypes"`
	ThreatEntries    []safeBrowsingRequestBodyThreatEntry `json:"threatEntries"`
}

type safeBrowsingRequestBodyThreatEntry struct {
	Url string `json:"url"`
}
