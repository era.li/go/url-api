package service

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

var sqsInstance *sqs.SQS

// SQS returns an instance of SQS
func SQS() *sqs.SQS {
	if sqsInstance == nil {
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			SharedConfigState: session.SharedConfigEnable,
		}))
		sqsInstance = sqs.New(sess)
	}

	return sqsInstance
}
