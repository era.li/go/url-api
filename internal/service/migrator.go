package service

import (
	"fmt"

	"github.com/dotronglong/goose/aws/dynamodb/migrations"
	"github.com/gobeat/interfaces"
	"gitlab.com/era.li/go/url-api/internal/repository/dynamodb"
)

// NewMigrator returns a migrator
func NewMigrator(workdir string) interfaces.Migrator {
	paths := make([]string, 1)
	paths[0] = fmt.Sprintf("%s/migration/url.json", workdir)
	return migrations.NewMigrator(dynamodb.DBInstance(), paths)
}
