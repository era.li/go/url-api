package constant

const (
	HeaderLocation         = "Location"
	HeaderContentType      = "Content-Type"
	HeaderAuthorization    = "Authorization"
	HeaderMockUser         = "Mock-User-Id"
	HeaderMockUserMetadata = "Mock-User-Metadata"
)
