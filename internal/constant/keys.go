package constant

const (
	KeyId             = "id"
	KeyQ              = "q"
	KeyPage           = "page"
	KeySize           = "size"
	KeyPagination     = "pagination"
	KeyItems          = "items"
	KeyPort           = "port"
	KeyAppEnv         = "APP_ENV"
	KeyUserMembership = "membership"

	KeyAwsRegion             = "AWS_REGION"
	KeyAwsDynamoEndpoint     = "AWS_DYNAMO_ENDPOINT"
	KeyAwsDynamoAccessId     = "AWS_DYNAMO_ACCESS_ID"
	KeyAwsDynamoAccessSecret = "AWS_DYNAMO_ACCESS_SECRET"
	KeyAwsSqsQueueURL        = "AWS_SQS_QUEUE_URL"

	KeyGcpProject = "GCP_PROJECT"

	KeyAuth0Domain         = "AUTH0_DOMAIN"
	KeyAuth0ClientId       = "AUTH0_CLIENT_ID"
	KeyAuth0ClientIdiOS    = "AUTH0_CLIENT_ID_IOS"
	KeyUrlIdLength         = "URL_ID_LENGTH"
	KeyRecaptchaSecret     = "RECAPTCHA_SECRET"
	KeyRecaptchaApiKey     = "RECAPTCHA_API_KEY"
	KeyPasscodeRedirectUrl = "PASSCODE_REDIRECT_URL"
	KeySafeBrowsingApiKey  = "SAFE_BROWSING_API_KEY"
	KeyWebappRedirectUrl   = "WEBAPP_REDIRECT_URL"

	KeyRequestPasscode = "passcode"
)
