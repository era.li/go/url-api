package constant

const (
	ColumnId        = "id"
	ColumnHash      = "hash"
	ColumnHref      = "href"
	ColumnUserId    = "user_id"
	ColumnTitle     = "title"
	ColumnTotalView = "total_view"
	ColumnCreatedAt = "created_at"
	ColumnUpdatedAt = "updated_at"
)
