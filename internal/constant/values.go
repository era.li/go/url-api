package constant

const (
	ValueTrue              = "true"
	ValueUserAnonymous     = "-"
	ValueAppTest           = "test"
	ValueMembershipPremium = "premium"
)
