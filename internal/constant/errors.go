package constant

import (
	"net/http"

	"github.com/gobeat/errors"
)

var (
	ResourceNotFoundError      = errors.NewHttpError("url.000", "Resource could not be found", http.StatusNotFound)
	DuplicateUrlIdError        = errors.NewHttpError("url.001", "Id already exists", http.StatusBadRequest)
	DuplicateUrlHashError      = errors.NewHttpError("url.002", "Url already exists", http.StatusBadRequest)
	InvalidHrefError           = errors.NewHttpError("url.003", "Invalid url's format", http.StatusBadRequest)
	InvalidRecaptchaError      = errors.NewHttpError("url.004", "Bot is detected", http.StatusForbidden)
	ResourceForbiddenError     = errors.NewHttpError("url.005", "Resource is forbidden", http.StatusForbidden)
	InvalidSafeUrlError        = errors.NewHttpError("url.006", "Unable to verify url", http.StatusInternalServerError)
	UnsafeUrlError             = errors.NewHttpError("url.007", "Unsafe url is detected", http.StatusBadRequest)
	InvalidUserMembershipError = errors.NewHttpError("url.008", "Invalid user's membership", http.StatusBadRequest)
	UnableToGenerateIdError    = errors.NewHttpError("url.009", "Internal Server Error", http.StatusInternalServerError)
)
