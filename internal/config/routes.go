package config

import (
	"context"
	"net/http"

	"github.com/dotronglong/goose"
	"github.com/dotronglong/goose/iam/auth0"
	"github.com/dotronglong/goose/recaptcha"
	rr "github.com/gobeat/router"

	"gitlab.com/era.li/go/url-api/internal/constant"
	"gitlab.com/era.li/go/url-api/internal/controller"
)

// RegisterRoutes registers necessary routes
func RegisterRoutes() http.Handler {
	return RegisterRoutesWithRouter(goose.RouterWithCors(nil))
}

// RegisterRoutesWithRouter registers routes with predefined router
func RegisterRoutesWithRouter(router rr.Router) http.Handler {
	amw := auth0.Middleware(goose.Env().GetString(constant.KeyAuth0Domain), func(r *http.Request) string {
		if r.Header.Get("Platform") == "ios" {
			return goose.Env().GetString(constant.KeyAuth0ClientIdiOS)
		}
		return goose.Env().GetString(constant.KeyAuth0ClientId)
	})
	asmw := func(w http.ResponseWriter, r *http.Request) (context.Context, error) {
		ctx, _ := amw(w, r)
		return ctx, nil
	}
	recaptchaAPIKey := goose.Env().GetOrDefault(constant.KeyRecaptchaApiKey, "")
	rcmw := recaptcha.Middleware(goose.Env().GetString(constant.KeyRecaptchaSecret), func(r *http.Request) bool {
		apiKey := r.Header.Get("x-api-key")
		if recaptchaAPIKey == "" || (apiKey != "" && apiKey != recaptchaAPIKey) {
			// skip this request if no recaptcha api key is configured
			// or request's api key is not identical recaptcha api key
			return true
		}

		return false
	})

	uc := new(controller.UrlController)
	router.GET("/to/:id", uc.Redirect)
	router.HEAD("/to/:id", uc.Redirect)
	router.GET("/urls", amw, uc.All)
	router.POST("/urls", rcmw, asmw, uc.Create)
	router.GET("/urls/:id", asmw, uc.Get)
	router.PATCH("/urls/:id", amw, uc.Update)
	router.DELETE("/urls/:id", amw, uc.Delete)
	router.POST("/urls/:id/passcode", amw, uc.CreatePasscode)
	router.DELETE("/urls/:id/passcode", amw, uc.DeletePasscode)

	return router
}

// RegisterRoutesRedirection registers routes for redirection
func RegisterRoutesRedirection() http.Handler {
	router := goose.Router()
	uc := new(controller.UrlController)
	router.GET("/:id", uc.Redirect)
	router.HEAD("/:id", uc.Redirect)

	return router
}
