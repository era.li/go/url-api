FROM alpine:3.9
RUN apk update && apk add ca-certificates
COPY bin /
EXPOSE 8080
CMD [ "/app" ]